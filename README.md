# Module that integrates stackly into Drupal.

1. My Stacks link options
  a. Add a link to any menu and make the path 'stackly/my-stacks'
  b. Add the class 'stackly-mystacks' to any link

2. Add stack it button
  a. Add panels ctools content type to any page with proper stackly meta tags


